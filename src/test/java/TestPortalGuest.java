
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@RunWith(ParallelParameterized.class)
public class TestPortalGuest extends TestBase {

    private static final String BASE_URL = "https://ayuda.paris.cl";

    public TestPortalGuest(MutableCapabilities capabilities) {
        super(capabilities);
    }

    @Test
    public void openPortal() {
        WebDriver webDriver = getDriver();
        webDriver.navigate().to(BASE_URL);
        Assert.assertEquals("Ayuda Paris.cl", webDriver.getTitle());
    }

    @Test
    public void seguimientoComproGuest() {
        WebDriver webDriver = getDriver();
        webDriver.navigate().to(BASE_URL);
        By selector = By.cssSelector(".container-nav > ul:nth-child(1) > li:nth-child(1) > a:nth-child(1)");
        WebElement element = webDriver.findElement(selector);
        element.click();
        Assert.assertEquals("Seguimiento de Orden | Ayuda Paris.cl", webDriver.getTitle());

    }

    @Test
    public void findOrderGuest() throws InterruptedException, IOException {
        WebDriver webDriver = getDriver();
        webDriver.navigate().to(BASE_URL);
        By selector = By.cssSelector(".container-nav > ul:nth-child(1) > li:nth-child(1) > a:nth-child(1)");
        WebElement element = webDriver.findElement(selector);
        element.click();

        selector = By.cssSelector(".inputCian");
        element = webDriver.findElement(selector);
        element.sendKeys("207742945");

        By buttonFind = By.cssSelector(".btn-buscar");
        element = webDriver.findElement(buttonFind);
        element.click();

        TimeUnit.SECONDS.sleep(5);

        TakesScreenshot scrShot =((TakesScreenshot)webDriver);
        File screenshotAs = scrShot.getScreenshotAs(OutputType.FILE);
        String browserName = ((RemoteWebDriver) webDriver).getCapabilities().getBrowserName();
        Date date = new Date();

        File file = new File("/home/pvd/" + browserName + "- "+ date.toString()  +".png");
        FileUtils.copyFile(screenshotAs, file);

        By textProduct = By.cssSelector(".containerProductInfo > strong:nth-child(1)");
        WebElement textProductElement = webDriver.findElement(textProduct);


        Assert.assertEquals(textProductElement.getText(), "Refrigerador Mademsa No Frost 251 Litros ALTUS 1250");

    }

}